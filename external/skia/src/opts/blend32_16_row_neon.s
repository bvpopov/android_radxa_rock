	.text
	.align
	.fpu neon
	.globl blend32_16_row_neon
	.func blend32_16_row_neon
	.include "Sk_neon_asm.h"

blend32_16_row_neon:
 
		push	{r4-r12}
		default_init_neon_regs
		and      r6,r0,#0xff
		lsr      r5,r0,#8
		and      r5,r5,#0xff 
		lsr		 r4,r0,#16
		and		 r4,r4,#0xff 
		lsr      r3,r0,#24
		and      r3,r3,#0xff 
		lsl		 r8,r6,#2
		lsl		 r9,r5,#3
		lsl 	 r10,r4,#2
					
		mov     r11,#0x100
		sub     r11,r11,r3
		lsr		r11,r11,#3 
		vdup.8  q8,r11 
			
		vmov.u8  q0,  #0xf8
		vmov.u8  q1,  #0x1f
		vmov.u8  q2,  #0x3f		
		lsr    r12,r2,#4
loop:	 
		vdup.16  q9,r8  
		vdup.16  q10,r8
		
		vdup.16  q11,r9 
		vdup.16  q12,r9
		
		vdup.16  q13,r10 
		vdup.16  q14,r10                 
		vld2.8 {q5, q6}, [r1] 
		
		pld [r1,#128]	
		
		vshl.u8 q7, q6, #3 
		vsri.u8 q7,q5,#5 
		
		vand.u8 q7,q7,q2
		
		vand.u8 q6, q6, q0 
		vshr.u8 q6,q6,#3 
		
		vand.u8 q5,q5,q1 
		    
		vmlal.u8 q9,d16,d12  
		vmlal.u8 q10,d17,d13
		
		vmlal.u8 q11,d16,d14        
		vmlal.u8 q12,d17,d15
		
		vmlal.u8 q13,d16,d10  
		vmlal.u8 q14,d17,d11
		
		vshl.u16 q9,q9,#6   
		vshl.u16 q10,q10,#6
		
		vshl.u16 q11,q11,#5 
		vshl.u16 q12,q12,#5
		
		vshl.u16 q13,q13,#6 
		vshl.u16 q14,q14,#6
		
		vsri.u16 q9,q11,#5
		vsri.u16 q9,q13,#11
		vsri.u16 q10,q12,#5
		vsri.u16 q10,q14,#11
		 
		vst1.16 {q9,q10},[r1]!
		   
		subs		r12, r12, #1		  
		bgt		loop   
		default_cleanup_neon_regs
		pop		{r4-r12}
		bx		lr
		
		.endfunc
		.end             