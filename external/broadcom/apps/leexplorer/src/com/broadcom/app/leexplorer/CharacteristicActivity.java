/******************************************************************************
 *
 *  Copyright (C) 2012 Broadcom Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ******************************************************************************/
package com.broadcom.app.leexplorer;

import java.util.UUID;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.broadcom.bt.gatt.BluetoothGattCharacteristic;

public class CharacteristicActivity extends Activity {

    public static final String EXTRA_NAME = "NAME";
    public static final String EXTRA_ADDR = "ADDRESS";
    public static final String EXTRA_SERVICE = "SERVICE";
    public static final String EXTRA_CHARACTERISTIC = "CHARACTERISTIC";

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;
    private static final int STATE_READY = 4;

    private static final String PROP_DIVIDER = " | ";

    private GattAppService mService = null;
    private String mAddress = null;
    private String mName = null;
    private UUID mServiceUuid = null;
    private UUID mCharUuid = null;
    private int mState = STATE_DISCONNECTED;
    private BluetoothGattCharacteristic mCharacteristic = null;
    private boolean mNotify = false;
    private boolean mIndicate = false;
    private ValueFragment mValueFragment = null;

    private final BroadcastReceiver GattStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(GattAppService.GATT_CONNECTION_STATE)) {
                String address = intent.getStringExtra(GattAppService.EXTRA_ADDR);
                if (address.equals(mAddress)) {
                    mState = intent.getBooleanExtra(GattAppService.EXTRA_CONNECTED, false) ?
                            STATE_CONNECTED : STATE_DISCONNECTED;
                }

            } else if (intent.getAction().equals(GattAppService.GATT_SERVICES_REFRESHED)) {
                if (mState == STATE_CONNECTED) mState = STATE_READY;

                mCharacteristic = mService.getCharacteristic(mAddress, mServiceUuid, mCharUuid);

                // If we can only do one operation, let's do it now...

                if ((mCharacteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_READ) != 0) {
                    mService.readCharacteristic(mCharacteristic);
                }

                if (mCharacteristic.getProperties() == BluetoothGattCharacteristic.PROPERTY_NOTIFY) {
                    setNotify(true);
                }

                if (mCharacteristic.getProperties() == BluetoothGattCharacteristic.PROPERTY_INDICATE) {
                    setIndicate(true);
                }
            } else if (intent.getAction().equals(GattAppService.GATT_CHARACTERISTIC_READ)) {
                final byte[] value = intent.getByteArrayExtra(GattAppService.EXTRA_VALUE);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mValueFragment.setValue(value);
                    }
                });
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateUi();
                }
            } );
        }
    };

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            mService = ((GattAppService.LocalBinder) rawBinder).getService();
            if (!mService.init()) {
                // TODO: Show a notice to the user here....
                finish();
                return;
            }

            mState = STATE_CONNECTING;
            mService.connect(mAddress);
            invalidateOptionsMenu();
        }

        @Override
        public void onServiceDisconnected(ComponentName classname) {
            mService = null;
        }
    };

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_characteristic);

        ActionBar actionBar = getActionBar();
        if (actionBar!= null) actionBar.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if (intent == null) return;

        mAddress = intent.getStringExtra(EXTRA_ADDR);
        mName = intent.getStringExtra(EXTRA_NAME);
        mServiceUuid = UUID.fromString(intent.getStringExtra(EXTRA_SERVICE));
        mCharUuid = UUID.fromString(intent.getStringExtra(EXTRA_CHARACTERISTIC));

        setTitle(mName);

        Intent bindIntent = new Intent(this, GattAppService.class);
        bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        mValueFragment = ValueFormatFactory.getFragment(getApplicationContext(), mCharUuid);
        fragmentTransaction.add(R.id.charFragment, mValueFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onDestroy() {
        unbindService(mServiceConnection);
        mService = null;
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(GattAppService.GATT_CONNECTION_STATE);
        intentFilter.addAction(GattAppService.GATT_SERVICES_REFRESHED);
        intentFilter.addAction(GattAppService.GATT_CHARACTERISTIC_READ);
        registerReceiver(GattStatusReceiver, intentFilter);

        if (mService != null) {
            mState = STATE_CONNECTING;
            mService.connect(mAddress);
        }
        invalidateOptionsMenu();
    }

    @Override
    public void onPause() {
        if (mService != null && mCharacteristic != null) {
            if (mNotify) mService.enableNotification(false, mCharacteristic);
            if (mIndicate) mService.enableIndication(false, mCharacteristic);
            mNotify = false;
            mIndicate = false;
        }

        unregisterReceiver(GattStatusReceiver);
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_characteristic, menu);

        boolean canRead = (mState == STATE_READY && mCharacteristic != null &&
                hasProp(mCharacteristic, BluetoothGattCharacteristic.PROPERTY_READ));
        menu.findItem(R.id.menu_read).setVisible(canRead);

        boolean canWrite = (mState == STATE_READY && mCharacteristic != null &&
                (hasProp(mCharacteristic, BluetoothGattCharacteristic.PROPERTY_WRITE)
                        || hasProp(mCharacteristic, BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE)));
        menu.findItem(R.id.menu_write).setVisible(canWrite);

        boolean canNotify = (mState == STATE_READY && mCharacteristic != null &&
                hasProp(mCharacteristic, BluetoothGattCharacteristic.PROPERTY_NOTIFY));
        menu.findItem(R.id.menu_notify).setVisible(canNotify);
        menu.findItem(R.id.menu_notify).setChecked(mNotify);

        boolean canIndicate = (mState == STATE_READY && mCharacteristic != null &&
                hasProp(mCharacteristic, BluetoothGattCharacteristic.PROPERTY_INDICATE));
        menu.findItem(R.id.menu_indicate).setVisible(canIndicate);
        menu.findItem(R.id.menu_indicate).setChecked(mIndicate);

        menu.findItem(R.id.menu_disconnect).setVisible(mState == STATE_READY);

        boolean bReconnect = (mService != null && mService.getReconnect(mAddress));
        menu.findItem(R.id.menu_reconnect).setVisible(mState == STATE_READY);
        menu.findItem(R.id.menu_reconnect).setChecked(bReconnect);

        mValueFragment.setReadonly(!canWrite);

        setProgressBarIndeterminateVisibility(mState == STATE_CONNECTING || mState == STATE_CONNECTED);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            Intent intent = new Intent(this, DeviceActivity.class);
            intent.putExtra(DeviceActivity.EXTRA_ADDR, mAddress);
            intent.putExtra(DeviceActivity.EXTRA_NAME, mName);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;

        case R.id.menu_connect:
            if (mService != null) {
                mService.connect(mAddress);
                mState = STATE_CONNECTING;
                invalidateOptionsMenu();
            }
            break;

        case R.id.menu_disconnect:
            if (mService != null) {
                mService.disconnect(mAddress);
            }
            break;

        case R.id.menu_read:
            if (mState == STATE_READY && mService != null && mCharacteristic != null) {
                mService.readCharacteristic(mCharacteristic);
            }
            break;

        case R.id.menu_write:
            if (mState == STATE_READY && mService != null && mCharacteristic != null) {
                if (mValueFragment.assignValue(mCharacteristic)) {
                    mService.writeCharacteristic(mCharacteristic);
                } else {
                    Toast.makeText(this, "Unable to assign characteristic value.", Toast.LENGTH_SHORT).show();
                }
            }
            break;

        case R.id.menu_notify:
            setNotify(!item.isChecked());
            break;

        case R.id.menu_indicate:
            setIndicate(!item.isChecked());
            break;

        case R.id.menu_reconnect:
            if (mService != null) {
                mService.setReconnect(mAddress, !item.isChecked());
                invalidateOptionsMenu();
            }
            break;

        default:
            break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setNotify(boolean enable) {
        if (mState == STATE_READY && mService != null && mCharacteristic != null) {
            mNotify = enable;
            mService.enableNotification(mNotify, mCharacteristic);
        } else {
            mNotify = false;
        }
        invalidateOptionsMenu();
    }

    private void setIndicate(boolean enable) {
        if (mState == STATE_READY && mService != null && mCharacteristic != null) {
            mIndicate = enable;
            mService.enableIndication(mIndicate, mCharacteristic);
        } else {
            mIndicate = false;
        }
        invalidateOptionsMenu();
    }

    private void updateUi() {
        invalidateOptionsMenu();

        AttributeLookup lookup = new AttributeLookup(getApplicationContext());

        if (mServiceUuid != null) {
            ((TextView)findViewById(R.id.textService)).setText(lookup.getService(mServiceUuid));
            ((TextView)findViewById(R.id.textServiceUuid)).setText(mServiceUuid.toString());
        }

        if (mCharUuid != null) {
            ((TextView)findViewById(R.id.textChar)).setText(lookup.getCharacteristic(mCharUuid));
            ((TextView)findViewById(R.id.textCharUuid)).setText(mCharUuid.toString());
        }

        if (mCharacteristic != null) {
            ((TextView)findViewById(R.id.textCharProps)).setText(propString(mCharacteristic));
        }
    }

    private boolean hasProp(BluetoothGattCharacteristic characteristic, int prop) {
        return ((characteristic.getProperties() & prop) == prop);
    }

    private String propString(BluetoothGattCharacteristic characteristic) {
        StringBuilder sb = new StringBuilder();

        if (hasProp(characteristic, BluetoothGattCharacteristic.PROPERTY_READ)) {
            if (sb.length() != 0) sb.append(PROP_DIVIDER);
            sb.append(getString(R.string.prop_read));
        }

        if (hasProp(mCharacteristic, BluetoothGattCharacteristic.PROPERTY_WRITE)
         || hasProp(mCharacteristic, BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE)) {
            if (sb.length() != 0) sb.append(PROP_DIVIDER);
            sb.append(getString(R.string.prop_write));
        }

        if (hasProp(mCharacteristic, BluetoothGattCharacteristic.PROPERTY_NOTIFY)) {
            if (sb.length() != 0) sb.append(PROP_DIVIDER);
            sb.append(getString(R.string.prop_notify));
        }

        if (hasProp(mCharacteristic, BluetoothGattCharacteristic.PROPERTY_INDICATE)) {
            if (sb.length() != 0) sb.append(PROP_DIVIDER);
            sb.append(getString(R.string.prop_indicate));
        }

        return sb.toString();
    }
}
