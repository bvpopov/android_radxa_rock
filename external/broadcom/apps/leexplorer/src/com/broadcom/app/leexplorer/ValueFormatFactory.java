/******************************************************************************
 *
 *  Copyright (C) 2012 Broadcom Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ******************************************************************************/
package com.broadcom.app.leexplorer;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import android.content.Context;
import android.util.Log;

import com.broadcom.app.leexplorer.fragments.DefaultFragment;

public class ValueFormatFactory {
    private static ValueFormatFactory mInstance = null;
    private Map<UUID, Class<?>> mClasses = null;

    private static final String FRAGMENTS_PACKAGE = "com.broadcom.app.leexplorer.fragments.";

    private ValueFormatFactory(Context context) {
        mClasses = new HashMap<UUID, Class<?>>();

        String[] uuids = context.getResources().getStringArray(R.array.fragments);
        for (String entry : uuids) {
            String[] parts = entry.split("\\|");
            if (parts.length == 2) {
                UUID uuid = UUID.fromString(parts[0]);
                String className = FRAGMENTS_PACKAGE + parts[1];
                try
                {
                    mClasses.put(uuid, Class.forName(className));
                }
                catch (ClassNotFoundException e) {
                    Log.e(MainActivity.TAG, ""+e);
                }
            }
        }
    }

    private static ValueFormatFactory getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ValueFormatFactory(context);
        }
        return mInstance;
    }

    public static ValueFragment getFragment(Context context, UUID uuid) {
        try
        {
            Class <?> fragmentClass = getInstance(context).mClasses.get(uuid);
            if (fragmentClass != null) return (ValueFragment)fragmentClass.newInstance();
        }
        catch (IllegalAccessException e)
        {
            Log.e(MainActivity.TAG, ""+e);
        }
        catch (InstantiationException e)
        {
            Log.e(MainActivity.TAG, ""+e);
        }

        Log.d(MainActivity.TAG, "getFragment() - No fragment for UUID " + uuid.toString());

        return new DefaultFragment();
    }
}
