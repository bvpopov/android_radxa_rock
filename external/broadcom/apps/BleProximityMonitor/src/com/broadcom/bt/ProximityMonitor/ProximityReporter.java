/************************************************************************************
 *
 *  Copyright (C) 2009-2011 Broadcom Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ************************************************************************************/
package com.broadcom.bt.ProximityMonitor;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.media.RingtoneManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
// BLTH01484916 ++
import android.util.Log;
// BLTH01484916 --

public class ProximityReporter {
// BLTH01484916 ++
    protected static final String TAG = "ProximityReporter";
// BLTH01484916 --
    public static final byte ALERT_LEVEL_NONE = 0;
    public static final byte ALERT_LEVEL_LOW  = 1;
    public static final byte ALERT_LEVEL_HIGH = 2;

    private static SQLiteDatabase db;
    private final static String[] PROJ = {
            "address", "nickname",
            "immediateAlertSupported", "remoteLinkLossAlertLevel", "remotePathLossAlertLevel", "remotePathLossThreshold",
            "localLinkLossAlertVibrate", "localLinkLossAlertSound", "localLinkLossAlertSoundName", "localLinkLossAlertSoundUri",
            "localPathLossAlertVibrate", "localPathLossAlertSound", "localPathLossAlertSoundName", "localPathLossAlertSoundUri",
            "longitude", "latitude", "accuracy", "timestamp", "enabled"
    };

    private static final String TABLE_NAME = "proximity_reporters";

    static HashMap<String,ProximityReporter> map = new HashMap<String,ProximityReporter>(); 

    // TODO: monitor battery level?
    
    // stored in database
    
    public String address;
    public String nickname;
    
    public boolean immediateAlertSupported;
    
    public byte remoteLinkLossAlertLevel;
    public byte remotePathLossAlertLevel;
    public byte remotePathLossThreshold;

    public boolean localLinkLossAlertVibrate;
    public boolean localLinkLossAlertSound;
    public String  localLinkLossAlertSoundName;
    public String  localLinkLossAlertSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM).toString();

    public boolean localPathLossAlertVibrate = true;
    public boolean localPathLossAlertSound;
    public String  localPathLossAlertSoundName;
    public String  localPathLossAlertSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM).toString();
    
    // for now, assume localPathLossThreshold == remotePathLossThreshold

    public boolean enabled = true;
    
    // location
    public double longitude, latitude;
    public float accuracy;
    public long timestamp;    
    // transient
    public int currentTxPowerLevel;    
    public boolean isConnected = false; 
    public Location cachedLocation;
    public long cachedDisconnectStartTime;    
    public String bluetoothDeviceName;

    public ProximityReporter(String address) {
        this.address = address;

        updateBluetoothDeviceName();
        map.put(address, this);
    }
        
    public ProximityReporter(Cursor cursor) {
        address = cursor.getString(0);
        nickname = cursor.getString(1);

        immediateAlertSupported = (cursor.getInt(2) != 0);
        remoteLinkLossAlertLevel = (byte) cursor.getInt(3);
        remotePathLossAlertLevel = (byte) cursor.getInt(4);
        remotePathLossThreshold = (byte) cursor.getInt(5);

        localLinkLossAlertVibrate = (cursor.getInt(6) != 0);
        localLinkLossAlertSound = (cursor.getInt(7) != 0);
        localLinkLossAlertSoundName = cursor.getString(8);
        localLinkLossAlertSoundUri = cursor.getString(9);
        
        localLinkLossAlertVibrate = (cursor.getInt(10) != 0);
        localLinkLossAlertSound = (cursor.getInt(11) != 0);
        localLinkLossAlertSoundName = cursor.getString(12);
        localLinkLossAlertSoundUri = cursor.getString(13);
        
        longitude = cursor.getDouble(14);
        latitude = cursor.getDouble(15);
        accuracy = cursor.getFloat(16);
        timestamp = cursor.getLong(17);

        enabled = (cursor.getInt(18) != 0);

        updateBluetoothDeviceName();
        map.put(address, this);
    }

    public void updateBluetoothDeviceName() {
        BluetoothDevice bd = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(address);
        if (bd != null)
            bluetoothDeviceName = bd.getName();
    }
    
    public String getName() {
        return (nickname != null && nickname.length() > 0) ? nickname : bluetoothDeviceName;
    }
    
    public ContentValues toContentValues(boolean includeAddress) {
        ContentValues values = new ContentValues();

        if (includeAddress)
            values.put("address"                , address);

        values.put("nickname"                       , nickname);
        
        values.put("immediateAlertSupported"        , immediateAlertSupported ? 1:0);
        values.put("remoteLinkLossAlertLevel"       , remoteLinkLossAlertLevel);
        values.put("remotePathLossAlertLevel"       , remotePathLossAlertLevel);
        values.put("remotePathLossThreshold"        , remotePathLossThreshold);

        values.put("localLinkLossAlertVibrate"      , localLinkLossAlertVibrate ? 1:0);
        values.put("localLinkLossAlertSound"        , localLinkLossAlertSound? 1:0);
        values.put("localLinkLossAlertSoundName"    , localLinkLossAlertSoundName);
        values.put("localLinkLossAlertSoundUri"     , localLinkLossAlertSoundUri);

        values.put("localPathLossAlertVibrate"      , localPathLossAlertVibrate ? 1:0);
        values.put("localPathLossAlertSound"        , localPathLossAlertSound? 1:0);
        values.put("localPathLossAlertSoundName"    , localPathLossAlertSoundName);
        values.put("localPathLossAlertSoundUri"     , localPathLossAlertSoundUri);

        values.put("longitude"                      , longitude);
        values.put("latitude"                       , latitude);
        values.put("accuracy"                       , accuracy);
        values.put("timestamp"                      , timestamp);
        
        values.put("enabled"                        , enabled? 1:0);

        return values;
    }

    public long insert() {
        return db.insert(TABLE_NAME, null, toContentValues(true));
    }

    public long update() {
        return db.update(TABLE_NAME, toContentValues(false), "address=?", new String[] { address });
    }
    
    public long delete() {
        map.remove(address);
        return db.delete(TABLE_NAME, "address=?", new String[] { address });
    }

    public static long delete(String address) {
        map.remove(address);
        return db.delete(TABLE_NAME, "address=?", new String[] { address });
    }
    
    public static long deleteAll() {
        map.clear();
        return db.delete(TABLE_NAME, null, null);
    }

    public static void initDatabase(Context context) {
        OpenHelper openHelper = new OpenHelper(context);
        db = openHelper.getWritableDatabase();
        loadAll();
    }

    public static void closeDatabase() {
        db.close();
    }


// BLTH01484916 ++
    public  static boolean isDevBonded(String address) {
        Log.d(TAG, "isDevBonded"); 
        BluetoothDevice bd = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(address);
        boolean isBonded = false;
        if (bd != null) {
            int bondState = bd.getBondState();
            Log.d(TAG, "isDevBonded address["+address+"] "+ "bondState = " + bondState); 
            if (bondState == BluetoothDevice.BOND_BONDED ) {
                isBonded = true;
            }
        }
        Log.d(TAG, "isDevBonded isBonded="+ isBonded); 
        return isBonded;
    }
// BLTH01484916 --

    public static void loadAll() {
        map.clear();
        Cursor cursor = db.query(TABLE_NAME, PROJ, null, null, null, null, "nickname desc");
        if (cursor.moveToFirst()) {
// BLTH01484916 ++
            boolean stayInDoLoop = true;
            do {
                if (isDevBonded(cursor.getString(0))) {
                ProximityReporter pr = new ProximityReporter(cursor);
//            } while (cursor.moveToNext());
                    Log.d(TAG, "create new ProximityReporter(" + cursor.getString(0)+")"); 
                    stayInDoLoop = cursor.moveToNext();
                } else {
                    Log.d(TAG, "loadAll device not bonded delete address" + cursor.getString(0)+")");  
                    db.delete(TABLE_NAME, "address=?", new String[] { cursor.getString(0)});
                    stayInDoLoop = cursor.moveToNext();
                }
                Log.d(TAG, "loadAll stayInDoLoop=(" + stayInDoLoop+")"); //sdh
            } while (stayInDoLoop);

        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }
    
    public static ProximityReporter get (String address) {
        return map.get(address);
    }
    
    public static boolean containsKey (String address) {
        return map.containsKey(address);
    }

    public static Collection<ProximityReporter> getAll() {
        return map.values();
    }
    
    private static class OpenHelper extends SQLiteOpenHelper {
        private static final String DATABASE_NAME = "ProximityMonitor.db";
        private static final int DATABASE_VERSION = 10;

        OpenHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + TABLE_NAME + " ("
                    + "address STRING, nickname STRING, "
                    + "immediateAlertSupported INTEGER, remoteLinkLossAlertLevel INTEGER, remotePathLossAlertLevel INTEGER, remotePathLossThreshold INTEGER,"
                    + "localLinkLossAlertVibrate INTEGER, localLinkLossAlertSound INTEGER, localLinkLossAlertSoundName STRING, localLinkLossAlertSoundUri STRING,"
                    + "localPathLossAlertVibrate INTEGER, localPathLossAlertSound INTEGER, localPathLossAlertSoundName STRING, localPathLossAlertSoundUri STRING,"
                    + "longitude REAL, latitude REAL, accuracy REAL, timestamp INTEGER, enabled INTEGER)");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }

    }

    
}
